const User = require('../models/User')
const userCtrl={
    getUsers:async()=>{
        return await User.findAll()        
    },
    findOne:async(req)=>{
        const {id}=req.params
        return await User.findByPk(id);
    },
    create:async(req)=>{
        const {nombre,email,password}=req.body
        return await User.create({nombre,email,password})
    },
    update:async(req)=>{
        const {nombre,email,password}=req.body
        const {id}=req.params
        return await User.update({nombre,email,password},{where:{id}})
    },
    remove:async(req)=>{
        const {id}=req.params
        return await User.destroy({where:{id}})
    }
}
module.exports=userCtrl;