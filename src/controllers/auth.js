const User =require('../models/User')
const jwt =require('jsonwebtoken')
const config =require('../config')
const comparePassword=async(password,user_password)=>{
  console.log('inputpss: '+password+' = '+user_password);
  return password === user_password
}
const signin = async (req, res) => {
    const {email}=req.body
    const user = await User.findOne({where:{ email}});
    if (!user) {
      return res.status(404).json({mensaje:"The email doesn't exists"});
    }
    console.log(user);
    const validPassword = await comparePassword(
      req.body.password,
      user.password
    );
    if (!validPassword) {
      return res.status(401).send({ auth: false, token: null });
    }
    const token = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: 60 * 60 * 24,
    });
    res.status(200).json({ auth: true, token });
  };
  const logout = async (req, res) => {
    res.header({'x-access-token':token}).status(200).send({ auth: false, token: null });
  };
  module.exports={signin,logout}