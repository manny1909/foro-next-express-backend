const Post = require('../models/Post')
const postCtrl={
    getPosts:async()=>{
        return await Post.findAll()        
    },
    getPostByUser:async(req)=>{
        const {id_user}=req.params
        return await Post.findAll({where:{id_user_fk:id_user}})
    },
    findOne:async(req)=>{
        const {id}=req.params
        return await Post.findByPk(id);
    },
    create:async(req)=>{
        const {titulo,parrafo,user}=req.body
        return await Post.create({titulo,parrafo,id_user_fk:user})
    },
    update:async(req)=>{
        const {titulo,parrafo,user}=req.body
        const {id}=req.params
        return await Post.update({titulo,parrafo,user},{where:{id}})
    },
    remove:async(req)=>{
        const {id}=req.params
        return await Post.destroy({where:{id}})
    }
}
module.exports=postCtrl;