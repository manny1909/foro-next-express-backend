const Comment = require('../models/Comment')
const commentCtrl={
    getComments:async(req)=>{
        const {id_post}=req.params
        return await Comment.findAll({where: {id_post_fk:id_post} })        
    },
    findOne:async(req)=>{
        const {id}=req.params
        return await Comment.findByPk(id);
    },
    create:async(req)=>{
        const {destinatario,post,parrafo,user}=req.body
        return await Comment.create({parrafo,id_user_fk:user,destinatario,id_post_fk:post})
    },
    update:async(req)=>{
        const {destinatario,post,parrafo,user}=req.body
        const {id}=req.params
        return await Comment.update({parrafo,id_user_fk:user,destinatario,id_post_fk:post},{where:{id}})
    },
    remove:async(req)=>{
        const {id}=req.params
        return await Comment.destroy({where:id})
    }
}
module.exports=commentCtrl;