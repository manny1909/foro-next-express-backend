const userCtrl=require('./user')
const postCtrl=require('./post')
const commentCtrl=require('./comment')
const auth=require('./auth')
const ctrl={
    userCtrl,postCtrl,commentCtrl,auth
}
module.exports=ctrl