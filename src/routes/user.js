const { Router }=require('express')
const router=Router();
const {userCtrl,auth}=require('../controllers/controller')
const verifyToken=require('../libs/verifyToken')
 router.route('/users')
    .get(verifyToken, async(req,res)=>{const users=await userCtrl.getUsers(); console.log('hecho'); res.json(users)})

router.route('/user/add')
    .post(async (req,res)=>{
        await userCtrl.create(req)
        res.json({mensaje:'created'})
    })
router.route('/user/:id')
    .get(async(req,res)=>{
        const data=await userCtrl.findOne(req)
        res.json(data)
    })
    .put(async(req,res)=>{
        await userCtrl.update(req)
        res.json({mensaje:'updated'})
    })
    .delete(async(req,res)=>{
        await userCtrl.remove(req)
        res.json({mensaje:'deleted'})
    })
router.route('/auth')
    .post(async(req,res)=>{
        await auth.signin(req,res)
    })    

module.exports=router;