const { Router }=require('express')
const router=Router();
const {postCtrl}=require('../controllers/controller');
const verifyToken = require('../libs/verifyToken');
router.post('/post/add',verifyToken, async(req)=>{
    await postCtrl.create(req)
    console.log('hecho');
})
router.route('/posts')
    .get(async(req,res)=>{const data=await postCtrl.getPosts(); console.log('hecho'); res.json(data)})


router.route('/post/:id')
    .get(async(req,res)=>{
        const data=await postCtrl.findOne(req)
        res.json(data)
    })
    .put(async(req,res)=>{
        await postCtrl.update(req)
        res.json({mensaje:'updated'})
    })
    .delete(async(req,res)=>{
        await postCtrl.remove(req)
        res.json({mensaje:'deleted'})
    })
    router.get('/posts/u/:id_user',async(req,res)=>{
        const data = await postCtrl.getPostByUser(req)
        res.json(data)
    })

module.exports=router;