const { Router }=require('express')
const router=Router();
const {commentCtrl}=require('../controllers/controller');
const verifyToken = require('../libs/verifyToken');
router.post('/comment/add',verifyToken, async(req)=>{
    await commentCtrl.create(req)
    console.log('hecho');
})
router.route('/comments/:id_post')
    .get(async(req,res)=>{const data=await commentCtrl.getComments(req); console.log('hecho'); res.json(data)})


router.route('/comment/:id')
    .get(async(req,res)=>{
        const data=await commentCtrl.findOne(req)
        res.json(data)
    })
    .put(async(req,res)=>{
        await commentCtrl.update(req)
        res.json({mensaje:'updated'})
    })
    .delete(async(req,res)=>{
        await commentCtrl.remove(req)
        res.json({mensaje:'deleted'})
    })

module.exports=router;
