const jwt =require('jsonwebtoken')
const config=require('../config')
module.exports=async(req,res,next)=>{
   console.log(req.headers);
    const token = req.headers["x-access-token"];

    if (!token) {
      return res
        .status(401)
        .send({ auth: false, message: "No Token aws Provided" });
    }
  
    const decoded = await jwt.verify(token, config.secret);
  
    req.userId = decoded.id;
    next();
}