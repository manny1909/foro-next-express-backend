require('dotenv').config()
const db=require('./models/database')
const app=require('./app')
const main=async()=>{
 app.set('port',process.env.PORT || 3000)
 await app.listen(app.get('port'))
 await db.authenticate()
 console.log('server on port '+app.get('port'));
}
main()