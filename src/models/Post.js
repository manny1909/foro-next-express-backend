const {DataTypes} = require('sequelize')
const db= require('./database')
const User = require('./User')
const Post=db.define('post',
{
    titulo:{
        allowNull:false,
        type:DataTypes.STRING(45)
    },
    parrafo:{
        allowNull:false,
    type:DataTypes.STRING(144)
    }
},{
     tableName:'post',
      timestamp:true
    })
User.hasMany(Post,{foreignKey:'id_user_fk'})
module.exports=Post;