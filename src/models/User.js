const db=require('./database.js') 
const { DataTypes }=require('sequelize')
const User= db.define('user',{
    
    nombre:{
        type:DataTypes.STRING(45),
        allowNull: false,
        },
    email:{
        type:DataTypes.STRING(60),
        allowNull:false,
        unique: true
    },
    password:{
        type: DataTypes.STRING(90),
        allowNull:false,

    },
},{
   timestamp: true,
   tableName: 'user',
 
})



module.exports=User;