require('dotenv').config()
const {Sequelize} = require('sequelize');
const {
    DB_HOST,
    DB_DATABASE,
    DB_USER,
    DB_PASSWORD
} = process.env;

console.log(DB_USER);
const db = new Sequelize(DB_DATABASE, DB_USER, DB_PASSWORD, {
    host: DB_HOST,
    dialect: 'postgres'
});
module.exports=db; 