const {DataTypes} = require('sequelize')
const db= require('./database')
const User = require('./User')
const Post = require('./Post')
const Comment=db.define('comment',
{
    parrafo:{
        allowNull:false,
    type:DataTypes.STRING(144)
    },
    destinatario:{
        type: DataTypes.STRING(50),
        allowNull:true
    }
},{
     tableName:'comment',
      timestamp:true
    })
User.hasMany(Comment,{foreignKey:'id_user_fk'})
Post.hasMany(Comment,{foreignKey:'id_post_fk'})
module.exports=Comment;